﻿namespace ConvertPres
{
    partial class frmConverter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdvwPolls = new System.Windows.Forms.DataGridView();
            this.btnPollData = new System.Windows.Forms.Button();
            this.btnConvert = new System.Windows.Forms.Button();
            this.lblRecNo = new System.Windows.Forms.Label();
            this.btnPollOrg = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdvwPolls)).BeginInit();
            this.SuspendLayout();
            // 
            // grdvwPolls
            // 
            this.grdvwPolls.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdvwPolls.Location = new System.Drawing.Point(12, 182);
            this.grdvwPolls.Name = "grdvwPolls";
            this.grdvwPolls.Size = new System.Drawing.Size(834, 260);
            this.grdvwPolls.TabIndex = 0;
            // 
            // btnPollData
            // 
            this.btnPollData.Location = new System.Drawing.Point(12, 152);
            this.btnPollData.Name = "btnPollData";
            this.btnPollData.Size = new System.Drawing.Size(91, 24);
            this.btnPollData.TabIndex = 1;
            this.btnPollData.Text = "Update Grid";
            this.btnPollData.UseVisualStyleBackColor = true;
            this.btnPollData.Click += new System.EventHandler(this.btnPollData_Click);
            // 
            // btnConvert
            // 
            this.btnConvert.Location = new System.Drawing.Point(12, 24);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(148, 23);
            this.btnConvert.TabIndex = 2;
            this.btnConvert.Text = "Update President PK";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // lblRecNo
            // 
            this.lblRecNo.AutoSize = true;
            this.lblRecNo.Location = new System.Drawing.Point(209, 29);
            this.lblRecNo.Name = "lblRecNo";
            this.lblRecNo.Size = new System.Drawing.Size(56, 13);
            this.lblRecNo.TabIndex = 3;
            this.lblRecNo.Text = "Rec No: 0";
            // 
            // btnPollOrg
            // 
            this.btnPollOrg.Location = new System.Drawing.Point(12, 71);
            this.btnPollOrg.Name = "btnPollOrg";
            this.btnPollOrg.Size = new System.Drawing.Size(148, 23);
            this.btnPollOrg.TabIndex = 4;
            this.btnPollOrg.Text = "Update PollOrg PK";
            this.btnPollOrg.UseVisualStyleBackColor = true;
            this.btnPollOrg.Click += new System.EventHandler(this.btnPollOrg_Click);
            // 
            // frmConverter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 473);
            this.Controls.Add(this.btnPollOrg);
            this.Controls.Add(this.lblRecNo);
            this.Controls.Add(this.btnConvert);
            this.Controls.Add(this.btnPollData);
            this.Controls.Add(this.grdvwPolls);
            this.Name = "frmConverter";
            this.Text = "Convert Presidential MDB";
            ((System.ComponentModel.ISupportInitialize)(this.grdvwPolls)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdvwPolls;
        private System.Windows.Forms.Button btnPollData;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.Label lblRecNo;
        private System.Windows.Forms.Button btnPollOrg;
    }
}

