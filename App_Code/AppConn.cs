using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;


namespace ConvertPres
{
    public class AppConn
    {
        
        public AppConn()
        {
        }

        public static string MakeConnection()
        {

            string connstring = "";
            try
            {
               // connstring = Globals.strDbConn;
                if (String.IsNullOrEmpty(connstring)) { connstring = " "; }
            }
            catch (Exception e)
            {
              //  ReUse getConnStr = new ReUse();
              //  connstring = getConnStr.GetConnectionString();
            }

            if (connstring.Trim().Length < 1)
            {
               // ReUse getConnStr = new ReUse();
              //  connstring = getConnStr.GetConnectionString();
            }
            return connstring;
        }
        /*
        public static OleDbConnection MakeOleConn()
        {
            string connstring = Globals.strDbConn;
            OleDbConnection cnnApplication = new OleDbConnection(connstring);
            cnnApplication.Open();
            return cnnApplication;

        }
         * */
        public static OleDbConnection MakeOleConn(string cnnstring)
        {
            OleDbConnection cnnApplication = new OleDbConnection(cnnstring);
            cnnApplication.Open();
            return cnnApplication;

        }
        public static SqlConnection MakeSQLConn()
        {
            SqlConnection cnnApplication = new SqlConnection(AppConn.MakeConnection());
            cnnApplication.Open();
            return cnnApplication;
        }
        public static SqlConnection MakeSQLConn(string strConnString)
        {
            if (String.IsNullOrEmpty(strConnString)) { strConnString = AppConn.MakeConnection(); }
            else {
            if (strConnString.Trim().Length < 10) { strConnString = AppConn.MakeConnection(); }
            }
            SqlConnection cnnApplication = new SqlConnection(strConnString);
            cnnApplication.Open();
            return cnnApplication;
        }
        /*
        public static SqlConnection MakeSQLConn(bool Asynch)
        {
            string connstring = Globals.strDbConn;
            SqlConnection cnnApplication = new SqlConnection(AppConn.MakeConnection());
            cnnApplication.Open();
            return cnnApplication;
        }
         * */
    }
}
