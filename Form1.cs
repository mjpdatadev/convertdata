﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Data.OleDb;

namespace ConvertPres
{
    public partial class frmConverter : Form
    {
        public frmConverter()
        {
            InitializeComponent();
        }

        private void btnPollData_Click(object sender, EventArgs e)
        {//te
            OleDbCommand sqlComm;
            OleDbConnection grdConn;
            OleDbDataReader drPolls;
            DataTable dtPolls = new DataTable();

            string strFilePath = "C:\\mpdata\\Cornell\\PresData\\PresRelate\\PresRate.mdb";
            string strConn = "";
            strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strFilePath + ";Persist Security Info=False;";
            grdConn = AppConn.MakeOleConn(strConn);
            sqlComm = grdConn.CreateCommand();
            sqlComm.CommandText = "SELECT * FROM tbl05Polls"; 
            drPolls = sqlComm.ExecuteReader(CommandBehavior.CloseConnection);
            dtPolls.Load(drPolls);
            grdvwPolls.DataSource = dtPolls;

        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            OleDbCommand sqlMaster;
            OleDbCommand sqlOldData;

            OleDbConnection grdConn;
            OleDbDataReader drPolls;
            OleDbDataReader drNewRec;
            DataTable dtMaster = new DataTable();
            DataTable dtPolls = new DataTable();

            string strFilePath = "C:\\mpdata\\Cornell\\PresData\\PresRelate\\PresRate.mdb";
            string strConn = "";
            strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strFilePath + ";Persist Security Info=False;";
            grdConn = AppConn.MakeOleConn(strConn);
            sqlMaster = grdConn.CreateCommand();
            sqlOldData = grdConn.CreateCommand();
            sqlMaster.CommandText = "SELECT * FROM tbl05Polls WHERE PresID IS NULL OR PresID<1";
//test            
drPolls = sqlMaster.ExecuteReader(CommandBehavior.CloseConnection);

            string PollDate = String.Empty;
            int iApprove = 0;
            int iDisApp = 0;
            int iNoOpin = 0;
            int iSample = 0;
            int iNewPK = 0;
            int iPresNo = 0;
            int iRecNum = 0;
            string strPres = String.Empty;
            bool doesMatch = true;

            if (drPolls.HasRows)
            {
                dtMaster.Load(drPolls);
                sqlOldData.CommandText = "SELECT * FROM tblZZCombine";
                if (grdConn.State == ConnectionState.Closed) { grdConn.Open(); }
                drNewRec = sqlOldData.ExecuteReader(CommandBehavior.CloseConnection);
                if (drNewRec.HasRows)
                {
                    dtPolls.Load(drNewRec);
                }

                foreach (DataRow rwMaster in dtMaster.Rows)
                {
                    iRecNum++;
                    Application.DoEvents();
                    Application.DoEvents();

                    lblRecNo.Text = "Rec No: " + iRecNum.ToString();
                    PollDate = "01/01/1899";
                    iApprove = -1;
                    iDisApp = -1;
                    iNoOpin = -1;
                    iNoOpin = -1;
                    iSample = -1;
                    iNewPK = -1;
                    PollDate = Convert.ToDateTime(rwMaster["PollStartDt"]).ToShortDateString();
                    iApprove = Int32.Parse(rwMaster["Approve"].ToString());
                    iDisApp = Int32.Parse(rwMaster["Disapprove"].ToString());
                    iNoOpin = Int32.Parse(rwMaster["NoOpinion"].ToString());
                    iSample = Int32.Parse((rwMaster["SampleSize"].ToString().Trim().Length < 1) ? "-1" : rwMaster["SampleSize"].ToString());
                    iNewPK = Int32.Parse(rwMaster["PollID"].ToString());
                    if (grdConn.State == ConnectionState.Closed) { grdConn.Open(); }
                    /*
                    if (iSample > -1)
                    {
                        sqlOldData.CommandText = "SELECT * FROM tblZZCombine WHERE [BegDate] = #" + PollDate + "# AND [Approve] = " + iApprove.ToString() +
                                " AND [Disapprove] = " + iDisApp.ToString() + " AND [No Opinion] = " + iNoOpin.ToString() + " AND [Sample Size] = " + iSample.ToString();
                    }
                    else
                    {
                        sqlOldData.CommandText = "SELECT * FROM tblZZCombine WHERE [BegDate] = #" + PollDate + "# AND [Approve] = " + iApprove.ToString() +
                                " AND [Disapprove] = " + iDisApp.ToString() + " AND [No Opinion] = " + iNoOpin.ToString();
                    }
                     * */
                    //drNewRec = sqlOldData.ExecuteReader(CommandBehavior.CloseConnection);
                    DataRow[] drPres;
                    drPres = dtPolls.Select("[BegDate] = #" + PollDate + "# AND [Approve] = " + iApprove.ToString() +
                                " AND [Disapprove] = " + iDisApp.ToString() + " AND [No Opinion] = " + iNoOpin.ToString());

                    strPres = "";
                    doesMatch = true;
                    //if (drNewRec.HasRows)
                    if (drPres.Length>0)
                    {
                        //dtPolls.Load(drNewRec);
                       // strPres = dtPolls.Rows[0]["President"].ToString();
                        strPres = drPres[0][3].ToString();
                        //foreach (DataRow rwPres in dtPolls.Rows)
                        for (int rwCnt = 0; rwCnt < drPres.Length - 1; rwCnt++)
                        {
                            //if (rwPres["President"].ToString() == strPres)
                            if (drPres[rwCnt]["President"].ToString() == strPres)

                            {
                                doesMatch = true;
                            }
                            else
                            {
                                doesMatch = false;
                                break;
                            }
                        }
                        if (doesMatch == true)
                        {
                            if (grdConn.State == ConnectionState.Closed) { grdConn.Open(); }
                            if (strPres.IndexOf("G.H.W.") < 0)
                            {
                                sqlOldData.CommandText = "SELECT [PresID] FROM tbl01Presidents WHERE LName LIKE '" + strPres + "'";
                            }
                            else 
                            {
                                sqlOldData.CommandText = "SELECT [PresID] FROM tbl01Presidents WHERE FName LIKE 'George W.' AND LName LIKE 'Bush'";
                            }
                            if (!drNewRec.IsClosed) { drNewRec.Close(); }
                            drNewRec = sqlOldData.ExecuteReader(CommandBehavior.CloseConnection);
                            iPresNo = -1;
                            if (drNewRec.HasRows)
                            {
                                drNewRec.Read();
                                iPresNo = Int32.Parse(drNewRec[0].ToString());
                            }
                            if (iPresNo > 0)
                            {
                                if (!drNewRec.IsClosed) { drNewRec.Close(); }
                                if (grdConn.State == ConnectionState.Closed) { grdConn.Open(); }
                                sqlOldData.CommandText = "UPDATE tbl05Polls SET [PresID] = " + iPresNo + " WHERE [PollID] = " + iNewPK;
                                int recs = sqlOldData.ExecuteNonQuery();
                            }
                            else
                            {
                                int Y = 0; //<---- there is no mathcing president in the Presidents table? Why?
                            }
                        }
                        else
                        {
                            int x = 0;   //<-------------- we have mismatching Presidents on multiple records. Why?
                        }
                    }
                }
            
            }

            MessageBox.Show("Done");
        }
        //===================================================================================================================================================
        private void btnPollOrg_Click(object sender, EventArgs e)
        {
            OleDbCommand sqlMaster;
            OleDbCommand sqlOldData;

            OleDbConnection grdConn;
            OleDbDataReader drPolls;
            OleDbDataReader drNewRec;
            DataTable dtMaster = new DataTable();
            DataTable dtPolls = new DataTable();

            string strFilePath = "C:\\mpdata\\Cornell\\PresData\\PresRelate\\PresRate.mdb";
            string strConn = "";
            strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strFilePath + ";Persist Security Info=False;";
            grdConn = AppConn.MakeOleConn(strConn);
            sqlMaster = grdConn.CreateCommand();
            sqlOldData = grdConn.CreateCommand();

            string strNewTabl = "SELECT tbl05Polls.PollID, tbl05Polls.PollOrgID, tbl01Presidents.LName, tbl05Polls.PresID, tbl05Polls.TermID," +
                " tbl05Polls.PollStartDt, tbl05Polls.PollEndDt, tbl05Polls.MonthOfTerm, tbl05Polls.SampleSize, tbl05Polls.Approve, tbl05Polls.Disapprove," +
                " tbl05Polls.NoOpinion, tbl05Polls.Active FROM tbl01Presidents INNER JOIN " +
                " tbl05Polls ON (tbl01Presidents.PresID = tbl05Polls.PresID) AND (tbl01Presidents.PresID = tbl05Polls.PresID) WHERE PollOrgID IS NULL OR PollOrgID<1";
            sqlMaster.CommandText = strNewTabl;
            drPolls = sqlMaster.ExecuteReader(CommandBehavior.CloseConnection);

            string PollDate = String.Empty;
            string PresLName = String .Empty;
            int iApprove = 0;
            int iDisApp = 0;
            int iNoOpin = 0;
            int iSample = 0;
            int iNewPK = 0;
            int iOrgNo = 0;
            int iRecNum = 0;
            string strPollOrg = String.Empty;
            string strListOrg = String.Empty;
            string strOldList = String.Empty;
            string strOldOrg = String.Empty;
            bool doesMatch = true;

            if (drPolls.HasRows)   //<----------------- Get all the rows from the new table tbl05Polls
            {
                dtMaster.Load(drPolls);     //<---------------  Load the rows into a datatbl from tbl05Polls
                sqlOldData.CommandText = "SELECT * FROM tblZZCombine";
                if (grdConn.State == ConnectionState.Closed) { grdConn.Open(); }
                drNewRec = sqlOldData.ExecuteReader(CommandBehavior.CloseConnection);
                if (drNewRec.HasRows)   //<-------------- Do the tha same for tblZZCombine.  Load the whole table locally into a datatable. This should be faster.
                {
                    dtPolls.Load(drNewRec);
                }

                foreach (DataRow rwMaster in dtMaster.Rows)
                {
                    iRecNum++;
                    lblRecNo.Text = "Rec No: " + iRecNum.ToString();
                    Application.DoEvents();
                    Application.DoEvents();

                    PollDate = "01/01/1899";
                    iApprove = -1;
                    iDisApp = -1;
                    iNoOpin = -1;
                    iNoOpin = -1;
                    iSample = -1;
                    iNewPK = -1;
                    PresLName = "ZZZZzzzzZZZZ";

                    //---  The dates, prez name, and poll Counts are our best links for inner joins betweeen the older table and new.   tblZZCombine and tbl05Polls.

                    PollDate = Convert.ToDateTime(rwMaster["PollStartDt"]).ToShortDateString();
                    iApprove = Int32.Parse(rwMaster["Approve"].ToString());
                    PresLName = rwMaster["LName"].ToString();
                    PresLName = PresLName.Substring(0, 4);
                    iDisApp = Int32.Parse(rwMaster["Disapprove"].ToString());
                    iNoOpin = Int32.Parse(rwMaster["NoOpinion"].ToString());
                    iSample = Int32.Parse((rwMaster["SampleSize"].ToString().Trim().Length < 1) ? "-1" : rwMaster["SampleSize"].ToString());
                    iNewPK = Int32.Parse(rwMaster["PollID"].ToString());
                    if (grdConn.State == ConnectionState.Closed) { grdConn.Open(); }

                    DataRow[] drPres;  //<-----  This is a new SELECT statmnt against the tblZZCombine based on dates, prez name, and poll Counts 
                    drPres = dtPolls.Select("[President] LIKE '" + PresLName + "%' AND  [BegDate] = #" + PollDate + "# AND [Approve] = " + iApprove.ToString() +
                                " AND [Disapprove] = " + iDisApp.ToString() + " AND [No Opinion] = " + iNoOpin.ToString());

                    strPollOrg = "";
                    strListOrg = "";
                    strOldList = "";
                    strOldOrg = "";
                    doesMatch = true;
                    if (drPres.Length > 0)
                    {
                        strListOrg = drPres[0]["ListOrg"].ToString();
                        strListOrg = StrFuzzy(strListOrg);
                        strPollOrg = drPres[0]["Organization"].ToString();
                        strPollOrg = StrFuzzy(strPollOrg);

                        for (int rwCnt = 0; rwCnt < drPres.Length - 1; rwCnt++)
                        {
                            strOldList = drPres[rwCnt]["ListOrg"].ToString();
                            strOldList = StrFuzzy(strOldList);
                            strOldOrg = drPres[rwCnt]["Organization"].ToString();
                            strOldOrg = StrFuzzy(strOldOrg);

                            if (strListOrg == strOldList && strPollOrg == strOldOrg)
                            {
                                doesMatch = true;
                            }
                            else
                            {
                                doesMatch = false;
                                break;
                            }
                        }
                        if (doesMatch == true)
                        {
                            if (grdConn.State == ConnectionState.Closed) { grdConn.Open(); }
                            if (String.IsNullOrEmpty(strListOrg))
                            {
                                if (strPollOrg.IndexOf("ris/Time/C") > -1) { strPollOrg = "Harris"; }
                                sqlOldData.CommandText = "SELECT [PollOrgID] FROM tbl11PollOrgs WHERE OrgName LIKE '%" + strPollOrg + "%'";
                            }
                            else
                            {
                                sqlOldData.CommandText = "SELECT [PollOrgID] FROM tbl11PollOrgs WHERE  ListOrg LIKE '%" + strListOrg + "%' AND OrgName LIKE '%" + strPollOrg + "%'";
                            }
                            if (!drNewRec.IsClosed) { drNewRec.Close(); }
                            if (grdConn.State == ConnectionState.Closed) { grdConn.Open(); }

                            drNewRec = sqlOldData.ExecuteReader(CommandBehavior.CloseConnection);
                            iOrgNo = -1;
                            if (drNewRec.HasRows)
                            {
                                drNewRec.Read();
                                iOrgNo = Int32.Parse(drNewRec[0].ToString());
                            }
                            if (strOldList.IndexOf("Washington") > -1 && strOldOrg == "B") { iOrgNo = 4; }  //shPos
                            if (strOldList.IndexOf("Washington") > -1 && strOldOrg == "shPos") { iOrgNo = 81; }  //
                            if (strOldList.IndexOf("lup") > -1 && strOldOrg == "allu") { iOrgNo = 28; }  //
                            if (strOldOrg.IndexOf("ra/pe", StringComparison.CurrentCultureIgnoreCase) > -1) { iOrgNo = 66; }  //
                            if (strOldOrg.IndexOf("today", StringComparison.CurrentCultureIgnoreCase) > -1) { iOrgNo = 78; }  //
                            if (strOldList.IndexOf("ovich", StringComparison.CurrentCultureIgnoreCase) > -1) { iOrgNo = 87; }  //
                            if (strOldOrg=="e") { iOrgNo = 56; }  //
                            if (iOrgNo > 0)
                            {
                              // iOrgNo = 52;
                                if (!drNewRec.IsClosed) { drNewRec.Close(); }
                                if (grdConn.State == ConnectionState.Closed) { grdConn.Open(); }
                                sqlOldData.CommandText = "UPDATE tbl05Polls SET [PollOrgID] = " + iOrgNo + " WHERE [PollID] = " + iNewPK;
                                int recs = sqlOldData.ExecuteNonQuery();
                            }
                            else
                            {
                                int Y = 0; //<---- there is no mathcing president in the Presidents table? Why?
                                Debug.Print(iNewPK.ToString() + " No Org Found: - " + strOldList + " - " + strOldOrg + " - " + strListOrg + " - " + strPollOrg);
                            }
                        }
                        else
                        {
                            int x = 0;   //<-------------- we have mismatching Presidents on multiple records. Why?
                            Debug.Print(iNewPK.ToString() + " - " + strOldList + " - " + strOldOrg + " - " + strListOrg + " - " + strPollOrg);
                            
                        }
                    }
                }

            }

            MessageBox.Show("Done");
        }

        private string StrFuzzy(string strToFix)
        {
            string retString = "";
            int iLen = 0;
            if (String.IsNullOrEmpty(strToFix))
            { return ""; }
            else
            {
                iLen = strToFix.Trim().Length;
                if (iLen > 10)
                {
                    strToFix = strToFix.Substring(3, iLen - 5).Trim();
                }
                else if (iLen > 7)
                {
                    strToFix = strToFix.Substring(2, iLen - 3).Trim();
                }
                else 
                {
                    strToFix = strToFix.Substring(1, iLen - 2).Trim();
                }
            }
            retString = strToFix;

            return retString;
        
        }

    }

}
